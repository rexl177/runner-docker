# runner-docker

gitlab cài đặt gitlab-runner sử dụng môi trường docker.

B1: Cài đặt gitlab-runner
Mở terminal, pull gitlab-runner:
    `docker pull gitlab/gitlab-runner:latest` 
Use Docker volumes to start the Runner container:

- Create the Docker volume:
    `docker volume create gitlab-runner-config`

- Start the GitLab Runner container using the volume we just created:
    `docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest`
(0)

B2: Register the runner
    `docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register`
- Enter the GitLab instance URL (for example, https://gitlab.com/): => điền link URL gitlab
- Enter the registration token: => Use the following registration token during setup
- Enter an executor: => chọn docker
- Enter the default Docker image => điền image docker được cung cấp ở (0)


 B3: docker restart gitlab-runner (restart gitlab-runner)
